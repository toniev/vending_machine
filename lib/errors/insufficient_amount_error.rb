class InsufficientAmountError < StandardError
  def initialize
    super('Please insert more coins.')
  end
end
