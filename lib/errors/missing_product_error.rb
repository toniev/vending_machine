class MissingProductError < StandardError
  def initialize(product_name)
    super("#{product_name} is not available.")
  end
end
