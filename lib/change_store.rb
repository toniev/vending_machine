require 'errors/insufficient_amount_error'

class ChangeStore
  attr_reader :change

  def initialize(change)
    @change = initialize_change(change)
  end

  def reload_change(change_to_reload)
    change.each do |denomination, _amount|
      next unless change_to_reload[denomination]

      change[denomination] += change_to_reload[denomination]
    end
  end

  def calculate_change_for(product_price, coins)
    total_in_pence = calculate_total_in_pence(coins)

    raise InsufficientAmountError if total_in_pence < product_price

    change_in_pence = total_in_pence - product_price

    change_from_available_coins(change_in_pence)
  end

  private

  DENOMINATIONS = {
    '£2' => 200,
    '£1' => 100,
    '50p' => 50,
    '20p' => 20,
    '10p' => 10,
    '5p' => 5,
    '2p' => 2,
    '1p' => 1
  }.freeze

  def initialize_change(change)
    @change = DENOMINATIONS.keys.each_with_object({}) do |denomination, memo|
      memo[denomination] = change[denomination] || 0

      memo
    end
  end

  def change_from_available_coins(change_in_pence)
    result = {}
    remaining_change = change_in_pence

    DENOMINATIONS.each do |denomination, denomination_value|
      coins_from_denomination = remaining_change / denomination_value
      available_from_denomination = usable_from_denomination(denomination, coins_from_denomination)
      change[denomination] -= available_from_denomination

      next if available_from_denomination.zero?

      remaining_change = remaining_change - (available_from_denomination * denomination_value)
      result[denomination] = available_from_denomination
    end

    result
  end

  def usable_from_denomination(denomination, coins_required)
    if change[denomination] >= coins_required
      coins_required
    else
      change[denomination]
    end
  end

  def calculate_total_in_pence(coins)
    raise InsufficientAmountError if coins.nil? || coins.empty?

    coins_total = 0

    coins.each do |denomination, amount|
      next unless DENOMINATIONS[denomination]

      coins_total += DENOMINATIONS[denomination] * amount
    end

    coins_total
  end
end
