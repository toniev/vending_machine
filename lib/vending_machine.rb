require 'errors/missing_product_error'
require 'change_store'

class VendingMachine
  attr_reader :products, :selected_product, :stock

  def initialize(products_to_stock, change)
    @products = products_prices(products_to_stock)
    @stock = populate_stock(products_to_stock)
    @change_store = ChangeStore.new(change)
  end

  def change
    change_store.change
  end

  def select_product(product_name)
    raise(MissingProductError, product_name) unless products[product_name]

    @selected_product = { product_name => products[product_name] }
  end

  def reload_products(products_to_reload)
    products_to_reload.each do |product|
      next unless stock[product[:name]]

      stock[product[:name]] += product[:stock]
    end
  end

  def reload_change(change_to_reload)
    change_store.reload_change(change_to_reload)
  end

  def receive_payment(coins)
    {
      product: selected_product.keys[0],
      change: change_store.calculate_change_for(selected_product.values[0], coins)
    }
  end

  private

  attr_reader :change_store

  def products_prices(products_to_stock)
    products_to_stock.each_with_object({}) do |product, available_products|
      available_products[product[:name]] = product[:price]
      available_products
    end
  end

  def populate_stock(products_to_stock)
    products_to_stock.each_with_object({}) do |product, stock|
      stock[product[:name]] = product[:stock]
      stock
    end
  end
end
