This repository contains some code that is designed to behave like a vending machine.

The main class is the `VendingMachine` class. It is used to interact with the vending machine. The class allows for several behaviours of the machine like initial load with products and change, reloading of these at a later stage, receiving and paying for a product and receiving change after paying for a product.

To run the code you would need:

- ruby 2.6.5

- `bundler` , you can get it by running `gem install bundler`

- running `bundle install` to install the `rspec` gem which I used for tesitng

To run the tests use `rspec`.

I have made several decision while implementing this and will try to explain my thinking and assumptions below:

1. As it is not mentioned in the test text I have not created any GUI or CLI for the vending machine.

2. I assumed that implementing a product stock and positioning logic as in how products are stored in or retrieved from different columns and rows of the machine is not needed as it is not mentioned in the test text. Because of that I have not implemented any such logic.

3. I have come up with a format of the data with which products and change are loaded into the machine. These can be seen in the vending machine spec.

4. A `ChangeStore` class is used to manage the machine's change. Since this is the biggest part of the logic of the test I decided to move the logic to this class so that there is a specialized class to deal with change. Arguably the change calculation logic can be moved to a `ChangeCalculator` service object but I decided to keep it here to minimize used time. Refactoring the logic out should be easy as I have tried to maintain minimum coupling.

5. I have kept the product storage logic in the `VendingMachine` class as it is just working with two simple hashes and I felt it is not necessary to be taken out at this point.

6. I did not create `Product` and `Coin` classes. I did not feel the need for these as they do not have any behaviour. Because of that I kept them as simple data types. But a downside of this is that at a few places usage of these data is a bit more verbose than if they would have been objects.

7. Decided to keep the product price and stock in different hashes for simplicity and speedier access to the data.

8. There are only tests that just target the behaviour of the VendingMachine class. They do indirectly test the logic of the `ChangeStore` but I purposefully did not add any more tests for that class so that some time is saved.

If there is something I have missed that you are finding needs an explaination I would be happy to further explain my thoughts over email or a call.

In the commits I have tried to show my thought process more than trying to make them look like production-ready commits.

I decided to give this implementation some more time than the expected as I found it interesting. But because of this the implementation might be a bit bigger than expected.

What would I do next:

- Implement unit tests for the change management logic in the `ChangeStore` class.

- Move the product and coin data to be stored in a `Product` and `Coin` classes to lower the usage of hashes and the verboseness.

- Move product storage to a `ProductStore` class so that the `VendingMachine` class does not have to have knowledge of that.

- Move the change calculation logic to a `ChangeCalculator` service object to lower the complexity in the `ChangeStore` as it should only be concerned with storage.
