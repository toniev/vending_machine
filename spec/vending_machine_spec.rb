require './lib/vending_machine'
require './lib/change_store'

RSpec.describe VendingMachine do
  let(:change) do
    {
      '1p' => 50,
      '2p' => 50,
      '5p' => 50,
      '10p' => 50,
      '20p' => 10,
      '50p' => 10,
      '£1' => 5,
      '£2' => 5
    }
  end
  let(:products) do
    [
      { name: 'Oreo', price: 85, stock: 20 },
      { name: 'Crisps', price: 80, stock: 10 }
    ]
  end
  let(:vending_machine) { VendingMachine.new(products, change) }

  context 'when the vending machine is initialized' do
    it 'the machine gets loaded with the supplied coins' do
      expect(vending_machine.change).to eq change
    end

    it 'the machine gets loaded with the supplied products' do
      expect(vending_machine.products).to eq({ 'Crisps' => 80, 'Oreo' => 85 })
    end
  end

  it 'shows current product stock' do
    expect(vending_machine.stock).to eq({ 'Crisps' => 10, 'Oreo' => 20 })
  end

  context 'when selecting a product' do
    let(:error_message) { 'Coca-Cola is not available.' }

    it 'allows for an item to be selected' do
      vending_machine.select_product('Oreo')

      expect(vending_machine.selected_product).to eq({ 'Oreo' => 85 })
    end

    it 'raises an error when selecting a non-existant item' do
      expect { vending_machine.select_product('Coca-Cola') }
        .to raise_error(MissingProductError, error_message)
    end
  end

  context 'when reloading products' do
    before { vending_machine.reload_products(products_to_reload) }

    context 'when reloading only existing items' do
      let(:products_to_reload) do
        [
          { name: 'Oreo', stock: 5 },
          { name: 'Crisps', stock: 5 },
        ]
      end

      it 'updates the product stock' do
        expect(vending_machine.stock).to eq({ 'Crisps' => 15, 'Oreo' => 25 })
      end
    end

    context 'when trying to reload new items' do
      let(:products_to_reload) { [{ name: 'Cheetos', stock: 10 }] }

      it 'does not add any new items' do
        expect(vending_machine.stock['Cheetos']).to be_nil
      end
    end
  end

  context 'when reloading change' do
    let(:additional_change) do
      {
        '20p' => 10,
        '50p' => 30
      }
    end
    let(:expected_change) do
      {
        '1p' => 50,
        '2p' => 50,
        '5p' => 50,
        '10p' => 50,
        '20p' => 20,
        '50p' => 40,
        '£1' => 5,
        '£2' => 5
      }
    end

    before { vending_machine.reload_change(additional_change) }

    it 'updates only the provided denominations' do
      expect(vending_machine.change).to eq expected_change
    end
  end

  context 'when paying for a product' do
    let(:selected_product) { 'Oreo' }

    before { vending_machine.select_product(selected_product) }

    context 'when not enough money is provided' do
      let(:error_message) { 'Please insert more coins.' }

      it 'throws insufficient funds error' do
        expect { vending_machine.receive_payment({ '5p' => 1 }) }
          .to raise_error(InsufficientAmountError, error_message)
      end
    end

    context 'when the provided money matches the price of the product' do
      let(:product_with_no_change) do
        {
          product: selected_product,
          change: {}
        }
      end
      let(:coins) do
        {
          '50p' => 1,
          '20p' => 1,
          '10p' => 1,
          '5p' => 1
        }
      end

      it 'returns product with no change' do
        expect(vending_machine.receive_payment(coins))
          .to eq(product_with_no_change)
      end
    end

    context 'when more money is provided than the product price' do
      let(:product_with_change) do
        {
          product: selected_product,
          change: { '10p' => 1, '5p' => 1, '£1' => 1 }
        }
      end
      let(:coins) { { '£2' => 1 } }

      it 'returns the right product and change' do
        expect(vending_machine.receive_payment(coins))
          .to eq(product_with_change)
      end
    end

    context 'when there is no more change in the machine' do
      let(:change) { {} }
      let(:product_with_no_change) do
        {
          product: selected_product,
          change: {}
        }
      end
      let(:coins) { { '50p' => 2 } }

      it 'returns product with no change' do
        expect(vending_machine.receive_payment(coins))
          .to eq(product_with_no_change)
      end
    end

    context 'when there is some change in the machine' do
      let(:change) { { '10p' => 1 } }
      let(:product_with_change) do
        {
          product: selected_product,
          change: { '10p' => 1 }
        }
      end
      let(:coins) { { '50p' => 2 } }

      it 'returns the right product and change' do
        expect(vending_machine.receive_payment(coins))
          .to eq(product_with_change)
      end
    end
  end
end
